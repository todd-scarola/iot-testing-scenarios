const AWS = require('aws-sdk');
const SQS = new AWS.SQS();

exports.handler = async(event) => {

   //loop through contacts
   for (let i = 0; i < event.length; i++) {
      var message = event[i];

      console.log('sending message: %j', message);

      //write JSON to SQS for processing
      await this.sendSqs(JSON.stringify(message));

   }

   return event;
};

//send message to SQS
exports.sendSqs = async function(message) {
   return new Promise(
      function(resolve, reject) {
         var params = {
            MessageBody: message,
            QueueUrl: process.env.SQS_URL
         };

         SQS.sendMessage(params, function(err, data) {
            if (err) {
               console.log('SQS error: %s', err);
               reject(err);
            }
            else {
               resolve(data);
            }
         });
      }
   );
};
